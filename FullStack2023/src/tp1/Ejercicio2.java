
package tp1;
import java.util.Scanner;
//Escribir un programa en el cual se le pregunte al usuario su nombre. A
//continuación, mostrar un mensaje que diga “Hola, ” completando el
//mensaje con el nombre que ingresó el usuario.
public class Ejercicio2 {

    public static void main(String[] args) {
        String nombre;
        Scanner s = new Scanner(System.in);
        System.out.println("Escriba su nombre: ");
        nombre = s.next();
        System.out.println("Hola " + nombre);
    }
    
}
