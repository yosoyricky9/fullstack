
package tp1;
import java.util.Scanner;

//Pedir al usuario que ingrese una temperatura en grados Celsius y
//mostrar por pantalla su equivalente en kelvin y grados Fahrenheit. Las
//fórmulas para conversiones son:
//Kelvin = 273,15 + Celsius
//Fahrenheit = (1,8 × Celsius) + 32
public class Ejercicio8 {

    public static void main(String[] args) {
        
        double Celsius,Fahrenheit,Kelvin;
    
        System.out.println("Ingrese una temperatura en Celsius: ");
        Scanner C = new Scanner(System.in);
        Celsius = C.nextDouble();
        
        Kelvin = 273.15 + Celsius;
        Fahrenheit = (1.8 * Celsius) + 32;
        
        System.out.printf("Valor ingresado: %.2f\u00B0C \n",Celsius);
        System.out.printf("Su equivalente en Kelvin es: %.2f\u00B0K \n",Kelvin);
        System.out.printf("Su equivalente en Fahrenheit es: %.2f\u00B0F \n",Fahrenheit);
    
       
    }
    
}
