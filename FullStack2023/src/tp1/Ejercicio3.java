
package tp1;
import java.util.Scanner;
//Escribir un programa que lea dos números y realice el cálculo de la
//suma, la resta, la multiplicación y la división entre ambos valores. Los
//resultados deben mostrarse por pantalla.
public class Ejercicio3 {

    public static void main(String[] args) {
       int a,b,sum,res,mult;
       double div;
       
       Scanner a1 = new Scanner(System.in);
       System.out.println("Ingrese el primer numero: ");
       a = a1.nextInt();
       
       Scanner a2 = new Scanner(System.in);
       System.out.println("Ingrese el segundo numero ");
       b = a2.nextInt();
       
       sum = a + b;
       res = a - b;
       mult = a * b;
       div = a / b;
       
        System.out.println("El resultado de la suma es: "+ sum + "\nEl resultado de la resta es: "+ res + "\nEl resultado de la multiplicacion es: " + mult + "\nEl resultado de la division es: " + div);
       
    }
    
}
