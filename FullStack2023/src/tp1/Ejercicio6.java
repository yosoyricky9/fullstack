
package tp1;
import java.util.Scanner;

//Pedir al usuario que ingrese el precio de un producto y el porcentaje
//de descuento. A continuación mostrar por pantalla el importe
//descontado y el importe a pagar
public class Ejercicio6 {

    public static void main(String[] args) {
        
    double precio,descuento,importe;
    int porcentaje;
    
    Scanner pre = new Scanner(System.in);
        System.out.println("Ingresa el precio de un producto: ");
        precio = pre.nextDouble();
        
    Scanner pctj = new Scanner (System.in);
        System.out.println("Ingresa el porcentaje a aplicar: ");
        porcentaje = pctj.nextInt();
        
        System.out.printf("Se va a aplicar el %d%% de descuento\n",porcentaje); 
    descuento = (precio*porcentaje) / 100;
    importe = precio - descuento;  
        System.out.printf("El importe descontado es: $%.2f\nEl importe a pagar es: $%.2f\n",descuento,importe);
    }
    
}
