
package tp1;
import java.util.Scanner;
//Pedir al usuario que ingrese el valor del radio de una circunferencia.
//Calcular y mostrar por pantalla el área y el perímetro. Recordá que el
//área y el perímetro se calculan con las siguientes fórmulas:

//area = PI × radio²
//perimetro = 2 × PI × radio

public class Ejercicio5 {

    public static void main(String[] args) {
        
       double radio,area,perimetro;
       
       Scanner s = new Scanner(System.in);
       System.out.println("Ingrese el radio de una circuferencia: ");
       radio = s.nextDouble();
       
       area = Math.PI * Math.pow(radio,2);
       perimetro = 2 * Math.PI * radio;
       
        System.out.printf("El area del numero ingresado es: %.2f y el perimetro es: %.2f ",area,perimetro);
       
       
    }
    
}
