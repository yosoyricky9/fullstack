
package tp1;
import java.util.Scanner;
//Escribir un programa que lea la estatura de tres personas, calcule el
//promedio de la altura de ellos y lo informe.
public class Ejercicio4 {

    public static void main(String[] args) {
        double p1,p2,p3;
        double promedio;
        
        
        Scanner s1 = new Scanner(System.in);
        System.out.println("Ingrese la altura en centimetros de la primer persona: ");
        p1 = s1.nextInt();
        
        Scanner s2 = new Scanner(System.in);
        System.out.println("Ingrese la altura en centimetros de la segunda persona");
        p2 = s2.nextInt();
        
        Scanner s3 = new Scanner(System.in);
        System.out.println("Ingrese la altura en centimetros de la tercer persona");
        p3 = s3.nextInt();
        
        promedio = (p1+p2+p3) / 3;
        
        System.out.println("El promedio de la altura de las 3 personas es " + promedio + "cm");
    }
    
}
