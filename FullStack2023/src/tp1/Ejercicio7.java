
package tp1;
import java.util.Scanner;

//Escribir un programa que reciba el valor de dos edades y las guarde
//en dos variables. Luego el programa debe intercambiar los valores de
//ambas variables y mostrarlas por pantalla. Por ejemplo, si el usuario
//ingresó los valores edad1 = 24 y edad2 = 35, el programa deberá
//mostrar edad1 = 35 y edad2 = 24
public class Ejercicio7 {

    public static void main(String[] args) {
        
    
        int edad1,edad2;
        
        System.out.println("Ingresa la edad de la primer persona: ");
        Scanner e1 = new Scanner(System.in);
        edad1 = e1.nextInt();
        
        System.out.println("Ingresa la edad de la segunda persona: ");  
        Scanner e2 = new Scanner(System.in);
        edad2 = e2.nextInt();
        
        System.out.printf("Edad de la primer persona: %d\nEdad de la segunda persona: %d\n",edad1,edad2);
        System.out.println("-------------------------------------\n");
        int temp = edad1;
        edad1 = edad2;
        edad2 = temp;
        
        System.out.printf("Edad de la primer persona: %d\nEdad de la segunda persona: %d\n",edad1,edad2);
    }
    
}
