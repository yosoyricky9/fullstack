
package tp1;
import java.util.Scanner;

//A partir de una cantidad de pesos que el usuario ingresa a través del
//teclado se debe obtener su equivalente en dólares, en euros, en
//guaraníes y en reales. Para este ejercicio se consideran las siguientes
//tasas:
//1 dólar = 231,68 pesos
//1 euro = 250,69 pesos
//1 peso = 31,00 guaraníes
//1 real = 46,81 pesos

public class Ejercicio9 {

    public static void main(String[] args) {
        
        double peso,euro,guaranies,real,dolar;
        
        System.out.println("Ingrese el peso a calcular:");
        Scanner P = new Scanner(System.in);
        peso = P.nextDouble();
        
        dolar = 231.68;
        euro = 250.69;
        guaranies =31.00;
        real = 46.81;
        
        // La conversión está mal. Por ejemplo, me dice que 100 pesos son 23168 dólares
        System.out.printf("$%.2f en dolares son:%.2f \n ",peso,peso*dolar);
        System.out.printf("$%.2f en euro son:%.2f \n",peso,peso*euro);
        System.out.printf("$%.2f en guaranies son:%.2f \n",peso,peso*guaranies);
        System.out.printf("$%.2f en reales son:%.2f \n",peso,peso*real);
        
       
    }
    
}
